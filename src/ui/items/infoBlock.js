import { DisplayItem } from "./displayItem";
import { RollOver } from 'ui/popups/itemPopup.vue';

import { SKILL, UNTAG } from '../../values/consts';

import Game from '../../game';
import { toRaw } from "vue";

/**
* Name to use for object in current context.
*/
export const DisplayName = (obj) => {

	let it = RollOver.context.getData(obj, false);
	return it ? it.name : obj;

}

export const CheckTypes = Object.freeze({
	COST: "cost",
	NEED: "need",
	FULL: "maxed",
	MOD: "mod" //TODO make mod check
})

/**
 * Convert item path display based on next subprop.
 * Certain properties indicate the display path should be treated specially,
 * such as switching the order of 'max' or omitting 'base' and 'value' display.
 */
var PathConversions = {

	effect: (rootPath) => rootPath,
	skipLocked: () => undefined,
	max: (rootPath) => 'max ' + rootPath,
	rate: (rootPath, subProp) => {

		//subProp = rootPath;

		let ind = rootPath.indexOf('.');
		if (ind > 0) {

			let baseItem = RollOver.context.getData(rootPath.slice(0, ind));
			if (baseItem && baseItem.type === SKILL) subProp = 'train ' + subProp + ' rate';

		} else return rootPath + ' ' + subProp;

		return subProp;
	}


};

PathConversions.mod = PathConversions.base = PathConversions.value = PathConversions.effect;

/**
* Convert display path based on current path object
* and current property being displayed.
* @param {string} rootPath - base path up to prop
* @param {string} prop - next prop on path - NOT path tail.
* @returns {string} path displayed. returns undefined if no information
* should be displayed for this variable path.
*/
export const ConvertPath = (rootPath, prop) => {

	let func = PathConversions[prop];
	if (func !== undefined) {

		// use conversion function.
		return func(rootPath, prop);

	} else {

		// no conversion func.
		if (prop.startsWith(UNTAG)) {
			prop = prop.slice(UNTAG.length);
			prop = DisplayName(prop);
			prop = "Existing " + prop;
		} else {
			prop = DisplayName(prop);
		}

		return rootPath ? rootPath + ' ' + prop : prop;

	}

}

/**
 * Collection of display info.
 */
export class InfoBlock {

	/**
	 * Attempt to add a path to the current item being referred to.
	 * @param {string} p
	 */
	static GetItem(p, curItem = null) {

		if (!curItem) return RollOver.context.getData(p, false);
		else return curItem[p] || curItem;

	}

	constructor() {

		this.results = {};

		/**
		 * @property {GData} rootItem - rootItem of the current path.
		 */
		//this.rootItem = null;

	}

	clear() {
		this.results = {};
	}

	add(itemName, value, isRate = false, checkType = null, ref = null, testfunc = null) {

		if (ref && ref.reverseDisplay) value = -value;

		let cur = this.results[itemName];
		let ctx = RollOver.context;

		if (cur === undefined) {
			let isAvailable = true;

			if (ref instanceof Object && checkType && toRaw(ctx) === Game) {

				if (checkType === CheckTypes.NEED && ref.fillsRequire instanceof Function) isAvailable &&= ref.fillsRequire(ctx);
				if (checkType === CheckTypes.COST && ref.canPay instanceof Function && !(ref.isRecipe || ref.instanced)) isAvailable &&= ref.canPay(value);
				if (checkType === CheckTypes.COST && (ref.isRecipe || ref.instanced)) {
					let CheckObj = {}
					CheckObj[ref.id] = value
					isAvailable &&= Game.canPay(CheckObj);
				}
				if (checkType === CheckTypes.FULL && ref.maxed instanceof Function) isAvailable &&= !ref.maxed();

			}
			if (checkType === CheckTypes.NEED && testfunc instanceof Function) {
				isAvailable &&= testfunc(Game.gdata, null, Game.state);
			}
			if (value.toString() != 0) this.results[itemName] = new DisplayItem(itemName, value, isRate, isAvailable);

		} else {

			cur.add(value);

		}

	}

}