const path = require('path');
const fs = require('fs');

const VueLoader = require('vue-loader');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const UiDir = path.resolve(__dirname, 'src/ui');
const DebugDir = path.resolve(__dirname, 'src/debug');

let packData = fs.readFileSync('package.json');
packData = JSON.parse(packData);
packData.version = "unstable " + packData.version;
const VERS_STR = JSON.stringify(packData.version);

const MakePlugins = (env, buildPath) => {
	const inProduction = env.production ? true : false;
	const plugins = [
		new VueLoader.VueLoaderPlugin({
			compilerOptions: {
				whitespace: 'condense'
			}
		}),
		new webpack.DefinePlugin({
			__DEBUG: true,
			__CHEATS: true,
			__DIST: env.production ? true : false,
			__CLOUD: env.cloud,
			__VERSION: VERS_STR,
			__VUE_OPTIONS_API__: 'true',
			__VUE_PROD_DEVTOOLS__: !inProduction,
			__VUE_PROD_HYDRATION_MISMATCH_DETAILS__: !inProduction
		}),
		new HtmlWebpackPlugin({
			template: 'index.ejs',
			title: "Theory of Magic",
			filename: path.resolve(buildPath, 'index.html'),
			__DIST: env.production ? true : false,
			__CLOUD: env.cloud
		}),
		new CopyPlugin({
			patterns: [
				{
					from: 'data',
					to: path.resolve(buildPath, 'data')
				},
				{
					from: 'css',
					to: path.resolve(buildPath, 'css')
				}
			]
		})
	];

	return plugins;
}

module.exports = (env, argv) => {

	const BuildPath = path.resolve(__dirname, argv['buildpath'] || 'dev');
	const __DIST = env.production ? true : false;

	return {
		mode: __DIST ? "production" : 'development',
		entry: {
			wizrobe: "./src/index.js"
		},
		module: {
			rules: [
				{
					test: /\.vue$/,
					include: [UiDir, DebugDir],
					loader: 'vue-loader',
					options: {
					}
				},
				{
					test: /\.css$/i,
					include: [path.resolve(__dirname, 'css'), UiDir, DebugDir],
					use: ['style-loader', 'css-loader']
				},
				{
					test: /\.tsx?$/,
					use: "ts-loader",
					exclude: "/node_modules/"
				}
			],
		},
		plugins: MakePlugins(env, BuildPath),
		output: {
			filename: "[name].js",
			chunkFilename: "[name].bundle.js",
			path: path.resolve(BuildPath, 'js/'),
			publicPath: 'js/',
			library: "[name]"
		},
		resolve: {
			modules: [
				path.resolve(__dirname, "src"),
				"node_modules"
			],
			extensions: ['.js', '.ts', '.tsx'],
			alias: {
				'modules': 'modules',
				'config': 'config',
				'data': '../data',
				'ui': 'ui',
				'remote': 'remote',
			}
		}
	}
}
